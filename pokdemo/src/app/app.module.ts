import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {PokeApiService} from './services/poke-api.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MyComponentComponent } from './my-component/my-component.component';
import { FormsModule } from '@angular/forms';
import { FilterPokemonPipePipe } from './filter-pokemon--pipe.pipe';
import {HttpClientModule} from '@angular/common/http';
import {NgxPaginationModule} from 'ngx-pagination';
import { PokeInfosComponent } from './poke-infos/poke-infos.component';
import {PokeDataService } from './services/poke-data.service';

@NgModule({
  declarations: [

    AppComponent,
    MyComponentComponent,
    FilterPokemonPipePipe,
    PokeInfosComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    NgxPaginationModule
  ],
  providers: [FilterPokemonPipePipe, PokeApiService, PokeDataService ],
  bootstrap: [AppComponent],
})
export class AppModule { }
