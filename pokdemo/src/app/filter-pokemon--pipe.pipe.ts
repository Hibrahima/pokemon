import { Pipe, PipeTransform } from '@angular/core';
import { Pokemon } from './pokemon';

@Pipe({
  name: 'filterPokemonPipe'
})
export class FilterPokemonPipePipe implements PipeTransform {

 /* transform(mesPokes: Pokemon[], searchString?: string): any {
    if (mesPokes.length!=0) {
      return mesPokes.filter(function(poke) {return poke.name.includes(searchString)});
    } else {
      return [];
    }
  }*/

  transform(mesPokes:Pokemon[], searchString?:string){
    if(searchString){

      searchString = searchString.toLowerCase();
      return mesPokes.filter(function(el:Pokemon){
        return el.name.toLowerCase().indexOf(searchString) > -1 || el.pok_id.toLowerCase().indexOf(searchString) > -1
    });
    }
    return mesPokes;
  }
}
