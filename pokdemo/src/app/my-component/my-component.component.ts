import { Component, OnInit } from '@angular/core';
import { Pokemon } from '../pokemon';
import { FilterPokemonPipePipe } from '../filter-pokemon--pipe.pipe';
import { PokeApiService } from '../services/poke-api.service';
import { PokeDataService } from '../services/poke-data.service';
@Component({
  selector: 'app-my-component',
  templateUrl: './my-component.component.html',
  styleUrls: ['./my-component.component.css']
})
export class MyComponentComponent implements OnInit {
  id;
  valeur: string;
  filtedPokes: Pokemon[];
  mesPokes: Pokemon[];
  pokemon: Pokemon;
  sharedId;
  p = 1;
  URL_API = 'https://pokeapi.co/api/v2/pokemon/';
  constructor(private pokesFilterPipe: FilterPokemonPipePipe, private pokeApiService: PokeApiService, private pokeData: PokeDataService) {
    this.mesPokes = [];
    this.pokeApiService.id.subscribe(id => {
      this.sharedId = id;
    });
  }

  ngOnInit() {
    this.pokeApiService.getAllPokemon().subscribe(dataPokemon => {
      // console.log(dataPokemon);
      dataPokemon['results'].map(pokemon => {
        this.mesPokes.push({
          name: pokemon.name,
          pok_id: pokemon.url.slice(this.URL_API.length, pokemon.url.length - 1)

        })
      })
    });

  }

  validerChoix(poke: Pokemon) {
    this.valeur = poke.name;
    this.id = poke.pok_id;

  };


  /*saisir() {
    this.filtedPokes = this.pokesFilterPipe.transform(this.mesPokes, this.valeur);
    console.log("some data " + this.valeur)
    this.mesPokes = this.mesPokes.filter(poke => poke.name.includes(this.valeur))
  }*/
  chooseMe(pok_id) {
    console.log(' id ' + pok_id);
    this.id = pok_id;
    this.pokeData.id.next(this.id); // transmettre une valeur au service grace à la methode next() de la librairie behaviorSubject
  }


}