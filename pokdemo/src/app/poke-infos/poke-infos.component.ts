import { Component, OnInit } from '@angular/core';
import { PokeApiService } from '../services/poke-api.service';
import { Pokemon } from '../pokemon';
import {PokeDataService} from '../services/poke-data.service';
@Component({
  selector: 'app-poke-infos',
  templateUrl: './poke-infos.component.html',
  styleUrls: ['./poke-infos.component.css']
})
export class PokeInfosComponent implements OnInit {
  mesPokes: Pokemon[];
  pokemon: Pokemon;
  URL_API = 'https://pokeapi.co/api/v2/pokemon/';
  constructor(private pokeApiService: PokeApiService,  private pokeData: PokeDataService ) {
    this.mesPokes = [];
    this.pokeApiService.getAllPokemon().subscribe(dataPokemon => {
      // console.log(dataPokemon);
      dataPokemon['results'].map(pokemon => {
        this.mesPokes.push({
          name: pokemon.name,
          pok_id: pokemon.url.slice(this.URL_API.length, pokemon.url.length - 1)

        });
      });
    });

  }

  ngOnInit() {
    this.pokeData.id.subscribe(id => {
      this.pokeApiService.getInfoPokemon(id).subscribe(info => {
        this.mesPokes.map(pokemon => {
          if (pokemon.pok_id == info['id']) {
            pokemon.stats = info['stats'];
            pokemon.taille = info['height'];
            pokemon.poids = info['weight'];
            pokemon.base_experience = info['base_experience'];
            pokemon.img = info['sprites'];
            this.pokemon = pokemon;
          }

        });
        console.log(this.pokemon);

      });
    });

  }



}
