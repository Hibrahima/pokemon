export class Pokemon {
    pok_id?: string;
    name: string;
    stats?: object[];
    poids?: string;
    taille?: string;
    img?: Object;
    base_experience?: Number;
}
