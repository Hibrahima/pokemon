import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class PokeApiService {
  id: BehaviorSubject<Number> = new BehaviorSubject(1) ;
 private  URL_API = 'https://pokeapi.co/api/v2/pokemon';

  constructor( private http: HttpClient) { }

getAllPokemon() {
 return this.http.get(this.URL_API + '?limit=964');
}

getInfoPokemon(id_poke) {
  return this.http.get(this.URL_API + '/' + id_poke);
}

}


